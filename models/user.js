'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    ten: DataTypes.STRING,
    maviber: DataTypes.STRING,
    maphongban: DataTypes.STRING,
    ngaysinh: DataTypes.DATE
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};